#!/bin/bash -l
#SBATCH --account=uzh8
#SBATCH --job-name="test"
#SBATCH --error="sum_omp_error.e"
#SBATCH --output="sum_omp_output.o"
#SBATCH --time=00:05:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-core=1
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=36
#SBATCH --partition=debug
#SBATCH --constraint=mc
#SBATCH --hint=nomultithread

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

srun sum_omp
